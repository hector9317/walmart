//
//  WalmartTests.swift
//  WalmartTests
//
//  Created by Hector Bautista on 22/09/23.
//

import XCTest
@testable import Walmart

final class WalmartTests: XCTestCase {
    var exerciseOne: ExerciseOne!
    
    override func setUp() {
        super.setUp()
        exerciseOne = ExerciseOne()
    }
    
    func testEvenSum() {
        let nums = [2,2,2,2,2]
        let expectedResult = 10
        let result = exerciseOne.sumEven(nums: nums)
        let resultHOF = exerciseOne.sumEvenHOF(nums: nums)
        XCTAssertEqual(result, expectedResult)
        XCTAssertEqual(resultHOF, expectedResult)
    }

    func testEvenSumEmptyInput() {
        let nums = [Int]()
        let expectedResult = 0
        let result = exerciseOne.sumEven(nums: nums)
        let resultHOF = exerciseOne.sumEvenHOF(nums: nums)
        XCTAssertEqual(result, expectedResult)
        XCTAssertEqual(resultHOF, expectedResult)
    }

    func testEvenSumIsZero() {
        let nums = [1,1,1,1,1]
        let expectedResult = 0
        let result = exerciseOne.sumEven(nums: nums)
        let resultHOF = exerciseOne.sumEvenHOF(nums: nums)
        XCTAssertEqual(result, expectedResult)
        XCTAssertEqual(resultHOF, expectedResult)
    }
}
