//
//  PersistenceController.swift
//  Walmart
//
//  Created by Hector Bautista on 22/09/23.
//

import CoreData
import Foundation

struct PersistenceController {
    static let shared = PersistenceController()
    let container: NSPersistentContainer
    
    var context: NSManagedObjectContext {
        return container.viewContext
    }
    
    private init() {
        container = NSPersistentContainer(name: DataModelKeys.name)
        container.loadPersistentStores { storeDescription, error in
            if let error = error as? NSError {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        }
        container.viewContext.automaticallyMergesChangesFromParent = true
    }
    
    func save() {
        do {
            try context.save()
        } catch {
            print("Coundn't save the data")
        }
    }
    
    func fetch() -> [ProductDBO] {
        let request = NSFetchRequest<ProductDBO>(entityName: DataModelKeys.ProductDBO)
        request.sortDescriptors = [NSSortDescriptor(key: DataModelKeys.id, ascending: true)]
        var products = [ProductDBO]()
        do {
            products = try context.fetch(request)
        } catch {
            print("Couldn't fetch products from DB")
        }
        return products
    }
    
    func addProducts(products: [Product]) {
        for product in products {
            let dbo = ProductDBO(context: context)
            dbo.category = product.category
            dbo.id = Int64(product.id)
            dbo.title = product.title
            dbo.price = product.price
        }
        save()
    }
}

