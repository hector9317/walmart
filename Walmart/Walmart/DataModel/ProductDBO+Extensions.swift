//
//  ProductDBO+Extensions.swift
//  Walmart
//
//  Created by Hector Bautista on 22/09/23.
//

import Foundation

extension ProductDBO {
    func getCodable() -> Product {
        return Product(id: Int(id),
                       title: title ?? "Unknown",
                       price: price,
                       category: category ?? "Unknown")
    }
}
