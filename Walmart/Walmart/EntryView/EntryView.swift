//
//  EntryView.swift
//  Walmart
//
//  Created by Hector Bautista on 22/09/23.
//

import Combine
import MapKit
import SwiftUI

struct EntryView: View {
    @ObservedObject var listViewModel: ListViewModel
    @ObservedObject var mapViewModel: MapViewModel
    @State var region: MKCoordinateRegion
    
    var body: some View {
        TabView {
            ListView(viewModel: listViewModel)
                .tabItem {
                    HStack {
                        VStack {
                            Image(systemName: "list.bullet.rectangle")
                            Text("Products")
                        }
                    }
                }
            MapaView(viewModel: mapViewModel, region: region)
                .tabItem {
                    HStack {
                        VStack {
                            Image(systemName: "map")
                            Text("Map")
                        }
                    }
                }
        }
    }
}

struct EntryView_Previews: PreviewProvider {
    static var previews: some View {
        let repository = RepositoryMock()
        let listViewModel = ListViewModel(repository: repository)
        
        let span = MKCoordinateSpan(latitudeDelta: 0.3, longitudeDelta: 0.3)
        let coordinate2D = CLLocationCoordinate2D(latitude: 19.4326, longitude: -99.1332)
        let region = MKCoordinateRegion(center: coordinate2D, span: span)
        let mapViewModel = MapViewModel(repository: repository)
        
        EntryView(listViewModel: listViewModel, mapViewModel: mapViewModel, region: region)
    }
}
