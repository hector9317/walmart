//
//  WalmartApp.swift
//  Walmart
//
//  Created by Hector Bautista on 22/09/23.
//

import MapKit
import SwiftUI

@main
struct WalmartApp: App {
    var body: some Scene {
        WindowGroup {
            let listRepository = Repository()
            let mapRepository = RepositoryMock()
            let listViewModel = ListViewModel(repository: listRepository)
            
            let span = MKCoordinateSpan(latitudeDelta: 0.3, longitudeDelta: 0.3)
            let coordinate2D = CLLocationCoordinate2D(latitude: 19.4326, longitude: -99.1332)
            let region = MKCoordinateRegion(center: coordinate2D, span: span)
            let mapViewModel = MapViewModel(repository: mapRepository)

            EntryView(listViewModel: listViewModel, mapViewModel: mapViewModel, region: region)
        }
    }
}
