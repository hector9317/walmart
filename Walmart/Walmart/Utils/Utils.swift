//
//  Utils.swift
//  Walmart
//
//  Created by Hector Bautista on 22/09/23.
//

import Foundation

enum EndPoints {
    static let products = "https://fakestoreapi.com/products"
}

enum RequestType {
    case FetchList
    case SatResults(String)
}

enum AppErrors: Error {
    case invalidURL
    case invalidResponse
}

enum DataModelKeys {
    static let name = "ProductsDataModel"
    static let id = "id"
    static let ProductDBO = "ProductDBO"
}
