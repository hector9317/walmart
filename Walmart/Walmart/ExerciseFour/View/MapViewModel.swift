//
//  MapViewModel.swift
//  Walmart
//
//  Created by Hector Bautista on 22/09/23.
//

import Combine
import Foundation

final class MapViewModel: ObservableObject {
    @Published var locations = [MapLocation]()
    let repository: RepositoryContract
    private var cancellable: AnyCancellable?
    
    init(repository: RepositoryContract) {
        self.repository = repository
    }

    func fetch() {
        cancellable = repository.fetch()
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { _ in
            }, receiveValue: { [weak self] (response: MapLocationResponse) in
                self?.locations = response.locations
            })
    }
}
