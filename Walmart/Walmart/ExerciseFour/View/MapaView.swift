//
//  MapaView.swift
//  Walmart
//
//  Created by Hector Bautista on 22/09/23.
//

import Foundation
import MapKit
import SwiftUI

struct MapaView: View {
    @ObservedObject var viewModel: MapViewModel
    @State var region: MKCoordinateRegion
    
    var body: some View {
        Map(coordinateRegion: $region,
            annotationItems: viewModel.locations) { location in
            MapMarker(coordinate: location.coordinate)
        }.onAppear {
            viewModel.fetch()
        }
    }
}

struct MapaViewPreview: PreviewProvider {
    static var previews: some View {
        let span = MKCoordinateSpan(latitudeDelta: 0.3, longitudeDelta: 0.3)
        let coordinate2D = CLLocationCoordinate2D(latitude: 19.4326, longitude: -99.1332)
        let region = MKCoordinateRegion(center: coordinate2D, span: span)
        let repository = RepositoryMock()
        let viewModel = MapViewModel(repository: repository)
        MapaView(viewModel: viewModel, region: region)
    }
}
