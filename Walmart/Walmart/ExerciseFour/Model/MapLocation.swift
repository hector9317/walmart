//
//  MapLocation.swift
//  Walmart
//
//  Created by Hector Bautista on 22/09/23.
//

import Foundation
import MapKit

struct MapLocationResponse: Codable {
    let locations: [MapLocation]

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self.locations = try container.decode([MapLocation].self)
    }
}

struct MapLocation: Identifiable, Codable {
    let id = UUID()
    let name: String
    let latitude: Double
    let longitude: Double
    var coordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    enum CodingKeys : String, CodingKey {
        case name
        case latitude
        case longitude
    }
}
