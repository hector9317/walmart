//
//  ExerciseOne.swift
//  Walmart
//
//  Created by Hector Bautista on 22/09/23.
//

import Foundation

class ExerciseOne {
    func sumEven(nums: [Int]) -> Int {
        var sum = 0
        for num in nums {
            if num % 2 == 0 {
                sum += num
            }
        }
        return sum
    }

    // HOF: Higher Order Functions
    func sumEvenHOF(nums: [Int]) -> Int {
        let filteredEven = nums.filter { $0 % 2 == 0 }
        return filteredEven.reduce(0, +)
    }
}
