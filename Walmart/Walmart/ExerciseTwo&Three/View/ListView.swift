//
//  ListView.swift
//  Walmart
//
//  Created by Hector Bautista on 22/09/23.
//

import Foundation
import SwiftUI

struct ListView: View {
    @ObservedObject var viewModel: ListViewModel

    var body: some View {
        NavigationView {
            List(viewModel.products) { product in
                VStack(alignment: .leading) {
                    Text(product.title)
                        .font(.subheadline)
                        .fontWeight(.semibold)
                     Text("$\(product.price)")
                        .font(.footnote)
                    Text(product.category.capitalized)
                        .font(.caption2)
                }
            }
        }
        .onAppear {
            viewModel.fetch()
        }
    }
}

struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        let repository = RepositoryMock()
        let viewModel = ListViewModel(repository: repository)
        return ListView(viewModel: viewModel)
    }
}
