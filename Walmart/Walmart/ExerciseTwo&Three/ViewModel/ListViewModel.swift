//
//  ListViewModel.swift
//  Walmart
//
//  Created by Hector Bautista on 22/09/23.
//

import Combine
import Foundation

final class ListViewModel: ObservableObject {
    @Published var products = [Product]()
    private let persistenceController = PersistenceController.shared
    private var cancellable: AnyCancellable?
    let repository: RepositoryContract
    
    init(repository: RepositoryContract) {
        self.repository = repository
    }
    
    func fetch() {
        let dbos = persistenceController.fetch()
        let mapped = dbos.map({ $0.getCodable() })
        if mapped.isEmpty {
            cancellable = repository.fetch()
                .receive(on: DispatchQueue.main)
                .sink(receiveCompletion: { _ in
                }, receiveValue: { [weak self] (response: ProductResponse) in
                    self?.persistenceController.addProducts(products: response.products)
                    self?.products = response.products
                })
        } else {
            self.products = mapped
        }
    }
}
