//
//  Product.swift
//  Walmart
//
//  Created by Hector Bautista on 22/09/23.
//

import Foundation

struct ProductResponse: Codable {
    let products: [Product]
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self.products = try container.decode([Product].self)
    }
}

struct Product: Identifiable, Codable {
    let id: Int
    let title: String
    let price: Double
    let category: String
}
