//
//  Repository.swift
//  Walmart
//
//  Created by Hector Bautista on 22/09/23.
//

import Combine
import Foundation

protocol RepositoryContract: AnyObject {
    func fetch<T: Codable>() -> AnyPublisher<T, Error>
}

final class Repository: RepositoryContract {
    func fetch<T: Codable>() -> AnyPublisher<T, Error> {
        let endpoint = EndPoints.products
        guard let url = URL(string: endpoint) else {
            return Fail(error: AppErrors.invalidURL).eraseToAnyPublisher()
        }
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .map(\.data)
            .decode(type: T.self, decoder: JSONDecoder())
            .mapError { _ in
                return AppErrors.invalidResponse
            }
            .eraseToAnyPublisher()
    }
}
